import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac023 on 2017/5/8.
  */
object WordCountApp extends App{
  val conf = new SparkConf().setAppName("WordCount").setMaster("local[*]")
  val sc = new SparkContext(conf)

  val lines=sc.textFile("/Users/mac023/Downloads/spark-doc.txt")
//  lines.take(10).foreach(println)
//  lines.map(str=>str.split(" ").toList).take(10).foreach(println)
//  lines.flatMap(str=>str.split(" ").toList).take(10).foreach(println)
  val words=lines.flatMap(i=>i.split(" "))

  //自己做的解答
//  words.map(str=>(str,1)).reduceByKey((acc,curr)=>acc+curr).take(10).foreach(println)

  //老師的正解
//  words.map(_->1).reduceByKey(_+_) //精簡版
  words.map(word=>word->1).reduceByKey((acc,curr)=>acc+curr)
//      .groupBy(str=>str).mapValues(_.size)
    .take(10)
    .foreach(println)


//  List("Mark","Spark","Rdd","Rdd").groupBy(str=>str).mapValues(_.size)
  readLine()

}
